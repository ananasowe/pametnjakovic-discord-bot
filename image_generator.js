const RandomGenerator = require('random-seed')
const Colour = require('color')
const Canvas = require('canvas')

const getOptimalFont = (canvas, text) => {
	const ctx = canvas.getContext('2d')
	let fontSize = 100
	do {
		// Assign the font to the context and decrement it so it can be measured again
		ctx.font = `bold ${fontSize -= 2}px serif` // sans-serif
	} while (ctx.measureText(text).width > canvas.width - 30)
	return { font: ctx.font, size: fontSize }
}

const pastelColour = (seed) => {
	const generator = RandomGenerator.create(seed)
	const hue = generator.range(360)
	return Colour(`hsl(${hue}, 55%, 40%)`)
}

const generateImage = async (word, partOfSpeach, colour) => {
	const dimensions = 300
	const canvas = Canvas.createCanvas(dimensions, dimensions)
	const ctx = canvas.getContext('2d')

	console.log('Generated colour: ' + colour)
	ctx.fillStyle = colour.rgb().toString()
	ctx.fillRect(0, 0, canvas.width, canvas.height)

	ctx.fillStyle = '#ffffff'
	const optimalFont = getOptimalFont(canvas, word)
	ctx.font = optimalFont.font
	const wordHeight = optimalFont.size
	const wordMeasure = ctx.measureText(word)

	console.log(`Word passed to generator: ${word}, ${partOfSpeach} Measuring: w ${wordMeasure.width}, h ${wordMeasure.height}/${wordHeight}`)
	ctx.fillText(word, (canvas.width / 2) - (wordMeasure.width / 2), (canvas.height / 2) + (wordHeight / 2))

	ctx.font = 'small-caps 20px serif'
	const partOfSpeachMeasure = ctx.measureText(partOfSpeach)
	ctx.fillText(partOfSpeach, (canvas.width / 2) - (partOfSpeachMeasure.width / 2), (canvas.height / 2) + wordHeight + 35)

	return canvas.toBuffer()
}

module.exports = {
	generateImage,
	pastelColour
}
