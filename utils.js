const Config = require('config')
const { Database, aql } = require('arangojs')
const dbConfig = Config.get('database')
const db = new Database(dbConfig)

module.exports = {
	shuffle: (a = []): [] => {
		var j, x, i
		for (i = a.length - 1; i > 0; i--) {
			j = Math.floor(Math.random() * (i + 1))
			x = a[i]
			a[i] = a[j]
			a[j] = x
		}
		return a
	},
	fetchRandomEntry: async (pos = ['noun'], filter = 0) => {
		try {
			var wiktionary = db.collection('wiktionary')
			pos = aql`[${aql.join(pos)}]`

			const parts = [
				aql`
				FOR doc IN ${wiktionary}
					FILTER doc.lang == "Serbo-Croatian"
					AND doc.pos IN ${pos}
					AND HAS(doc, "senses") AND HAS(doc, "word") AND HAS(doc.senses[0], "glosses")`,
				aql`
					SORT RAND()
					LIMIT 1
					RETURN {
						id: CONCAT(doc.word, "_", doc._key),
						word: doc.word, 
						heads: doc.heads, 
						senses: doc.senses, 
						synonyms: doc.synonyms, 
						conjugation: doc.conjugation, 
						pos: doc.pos
					}
			`]
			switch (filter) {
			case 1:
				parts.splice(1, 0, aql`AND REGEX_MATCHES(doc.word, "^[\u0400-\u04FF$&+,:;=?@#|'<>.^*()%!-]", false)`)
				break
			case 2:
				parts.splice(1, 0, aql`AND REGEX_MATCHES(doc.word, "[\u0000-\u007F\u0100-\u017F]", false)`)
				break
			}
			const query = aql.join(parts)
			console.log(`Query: ${query.query}`)
			const wotd = await db.query(query)
			return await wotd.next()
		} catch (err) {
			throw new Error(err.message)
		}
	},
	fetchTargetEntry: async (entry = '0') => {
		try {
			var wiktionary = db.collection('wiktionary')
			const query = aql`FOR doc IN ${wiktionary}
					FILTER doc._key == ${entry}
					RETURN {
						id: CONCAT(doc.word, "_", doc._key),
						word: doc.word, 
						heads: doc.heads, 
						senses: doc.senses, 
						synonyms: doc.synonyms, 
						conjugation: doc.conjugation, 
						pos: doc.pos
					}`
			const result = await db.query(query)
			return await result.next()
		} catch (err) {
			throw new Error(err.message)
		}
	},
	fetchEntryInfo: async (entry = '0') => {
		try {
			var wiktionary = db.collection('wiktionary')
			const query = aql`FOR doc IN ${wiktionary}
					FILTER doc._key == ${entry}
					RETURN doc`
			const result = await db.query(query)
			return await result.next()
		} catch (err) {
			throw new Error(err.message)
		}
	}
}
