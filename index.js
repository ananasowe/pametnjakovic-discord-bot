const Config = require('config')
const { Database, aql } = require('arangojs')
const Discord = require('discord.js')
const ImageGenerator = require('./image_generator')

const dbConfig = Config.get('database')
const discordConfig = Config.get('discord')
const db = new Database(dbConfig)
const client = new Discord.Client()

const entryTypeMap = new Map([
	['adv', 'adverb'],
	['adj', 'adjective'],
	['prep', 'preposition'],
	['pron', 'pronoun'],
	['intj', 'interjection'],
	['conj', 'conjunction'],
	['num', 'numeral'],
	['det', 'determiner'],
	['punct', 'punctuation mark'],
	['abbrev', 'abbreviation'],
	['prep_phrase', 'prepositional phrase'],
	['combining_form', 'combining form']
])

const goodNight = [
	'laku noć',
	'laku noc',
	'lijepo spavaj',
	'лаку ноћ',
	'лаку ноц',
	'dobrou noc',
	'goodnight',
	'good night',
	'ć'
]

//const commands = []
//
//var normalizedPath = require('path').join(__dirname, 'commands/')
//console.log('Detected command location: ' + normalizedPath)
//require('fs').readdirSync(normalizedPath).forEach(function (file) {
//	console.log('File: ' + normalizedPath + file)
//	commands.push(require(normalizedPath + file))
//})

const icons = Array.from('🍏🍎🍐🍊🍋🍌🍉🍇🍓🍈🍒🍑🥭🍍🥥🥝🍅🥑🍆🌶🥒🥬🥦🧄🧅🌽🥕🥗🥔🍠🥜🍯🍞🥐🥖🥨🥯🥞🧇🧀🍗🍖🥩🍤🥚🍳🥓🍔🍟🌭🍕🍝🥪🌮🌯🥙🧆🍜🥘🍲🥫🧂🧈🍥🍣🍱🍛🍙🍚🍘🥟🍢🍡🍧🍨🍦🍰🎂🧁🥧🍮🍭🍬🍫🍿🍩🍪🥠🥮☕🍵🥣🍼🥤🧃🧉🥛🍺🍻🍷🥂🥃🍸🍹🍾🍶🧊🥄🍴🥢🥡')

const shuffle = (a) => {
	var j, x, i
	for (i = a.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1))
		x = a[i]
		a[i] = a[j]
		a[j] = x
	}
	return a
}
const fetchRandomEntry = async (pos = ['noun'], filter = 0) => {
	try {
		var wiktionary = db.collection('wiktionary')
		pos = aql`[${aql.join(pos)}]`

		const parts = [
			aql`
			FOR doc IN ${wiktionary}
				FILTER doc.lang == "Serbo-Croatian"
				AND doc.pos IN ${pos}
				AND HAS(doc, "senses") AND HAS(doc, "word") AND HAS(doc.senses[0], "glosses")`,
			aql`
				SORT RAND()
				LIMIT 1
				RETURN {
					id: CONCAT(doc.word, "_", doc._key),
					word: doc.word, 
					heads: doc.heads, 
					senses: doc.senses, 
					synonyms: doc.synonyms, 
					conjugation: doc.conjugation, 
					pos: doc.pos
				}
		`]
		switch (filter) {
		case 1:
			parts.splice(1, 0, aql`AND REGEX_MATCHES(doc.word, "^[\u0400-\u04FF$&+,:;=?@#|'<>.^*()%!-]", false)`)
			break
		case 2:
			parts.splice(1, 0, aql`AND REGEX_MATCHES(doc.word, "[\u0000-\u007F\u0100-\u017F]", false)`)
			break
		}
		const query = aql.join(parts)
		console.log(`Selected query filter: ${filter}`)
		const wotd = await db.query(query)
		return await wotd.next()
	} catch (err) {
		throw new Error(err.message)
	}
}

const fetchTargetEntry = async (entry) => {
	try {
		var wiktionary = db.collection('wiktionary')
		const query = aql`FOR doc IN ${wiktionary}
				FILTER doc._key == ${entry}
				RETURN {
					id: CONCAT(doc.word, "_", doc._key),
					word: doc.word, 
					heads: doc.heads, 
					senses: doc.senses, 
					synonyms: doc.synonyms, 
					conjugation: doc.conjugation, 
					pos: doc.pos
				}`
		const result = await db.query(query)
		return await result.next()
	} catch (err) {
		throw new Error(err.message)
	}
}

const fetchEntryInfo = async (entry) => {
	try {
		var wiktionary = db.collection('wiktionary')
		const query = aql`FOR doc IN ${wiktionary}
				FILTER doc._key == ${entry}
				RETURN doc`
		const result = await db.query(query)
		return await result.next()
	} catch (err) {
		throw new Error(err.message)
	}
}

const generateWotdEmbed = async (word, dictionaryEntry) => {
	const partOfSpeach = entryTypeMap.has(dictionaryEntry.pos) ? entryTypeMap.get(dictionaryEntry.pos) : dictionaryEntry.pos
	const colour = ImageGenerator.pastelColour(partOfSpeach)

	const attachment = new Discord.MessageAttachment(await ImageGenerator.generateImage(word, partOfSpeach, colour), 'wotd_sticker.png')
	const description = partOfSpeach.charAt(0).toUpperCase() + partOfSpeach.slice(1) + '\n' +
		dictionaryEntry.senses.map(sense => {
			return '• ' + sense.glosses.join(', ') + ' ' +
			((sense.tags !== undefined) ? '[*' + sense.tags.join(', ') + '*]' : '')
		}).join('\n')
	const wotdEmbed = new Discord.MessageEmbed()
		.setTitle(word)
		.setColor(colour.hex())
		.attachFiles(attachment)
		.setDescription(description)
		.setImage('attachment://wotd_sticker.png')

	return wotdEmbed
}

const generatePollEmbed = async (user, question, answers) => {
	const answerlist = answers.map((answer, index) => icons[index] + ' → ' + answer).join('\n')
	const pollEmbed = new Discord.MessageEmbed()
		.setTitle(question)
		.setColor('#BCD171')
		.setDescription(answerlist)
		.setAuthor(user.tag, user.displayAvatarURL())

	return pollEmbed
}

// Events:
client.once('ready', () => {
	console.log('Ready!')
})

client.on('guildCreate', function (guild) {
	console.log(`Client joined a guild named ${guild.name}`)
	guild.systemChannel.send(new Discord.MessageEmbed().setTitle('🏠').setDescription('Feels like home 😌'))
})

client.on('message', msg => {
	if (msg.content.startsWith(discordConfig.prefix + 'fetch')) {
		if (msg.member.hasPermission('ADMINISTRATOR')) {
			(async () => {
				const processingMessage = await msg.reply('⏳ Processing...')
				const channel = msg.channel
				console.log(`Processing a fetch command ran by ${msg.member.user.tag}`)

				// Map each word after plus (a parameter) to a matching entry from entryTypeMap. Get all entries (...entryTypeMap.entries())
				// filter them, taking the value (field [1]) if it matches the word. Concatenate an empty value [['']] to the result, in case it's empty
				// and return the second nasted array (2x .shift()) or a word if no matches have been found
				const parameters = msg.content.split('+').slice(1)
					.map(word => ([...entryTypeMap.entries()].filter(value => value[1] === word).concat([['']]).shift().shift() || word))

				if (parameters.findIndex(i => i.startsWith('op_clean')) > -1) {
					msg.delete()
				}
				const queryParameters = parameters.filter(entry => !entry.startsWith('op_'))
				let filter = 0
				if (parameters.findIndex(i => i.startsWith('op_cyrillic')) > -1) {
					filter = 1
				} else if (parameters.findIndex(i => i.startsWith('op_latin')) > -1) {
					filter = 2
				}
				try {
					const entry = await fetchRandomEntry(queryParameters.length > 0 ? queryParameters : undefined, filter)
					const word = (entry.heads != null && entry.heads[0].head !== undefined) ? entry.heads[0].head : entry.word
					console.log('Passed parameters: ' + parameters)
					console.log('Fetched entry: ' + JSON.stringify(entry))
					await channel.send(await generateWotdEmbed(word, entry))
					processingMessage.delete()
					if (parameters.findIndex(i => i.startsWith('op_update')) > -1) {
						channel.setName(`🎁wotd-${word}`).then(console.log('Renamed the channel')).catch(error => console.error(error))
					}
				} catch (exception) {
					console.error(exception)
					processingMessage.edit('Failed to fetch the entry :( Empty result.')
				}
			})()
		} else {
			msg.reply("I'm sorry. Only administrators are allowed to use that command :(")
		}
	} else if (msg.content.startsWith(discordConfig.prefix + 'find')) {
		if (msg.member.hasPermission('ADMINISTRATOR')) {
			(async () => {
				const channel = msg.channel
				console.log(`Processing a search command ran by ${msg.member.user.tag}`)

				const parameter = msg.content.split('+').slice(1).shift()
				// msg.delete()
				try {
					const entry = await fetchTargetEntry(parameter)
					const word = (entry.heads != null && entry.heads[0].head !== undefined) ? entry.heads[0].head : entry.word
					console.log('Fetched targeted entry: ')
					console.log(JSON.stringify(entry, null, 2))
					await channel.send(await generateWotdEmbed(word, entry))
					// channel.setName(`🎁wotd-${word}`).then(console.log('Renamed the channel')).catch(error => console.error(error))
				} catch (exception) {
					console.error(exception)
					msg.channel.send('Entry either doesn\'t exist or doesn\'t contain enough information :(')
				}
			})()
		} else {
			msg.reply("I'm sorry. Only administrators are allowed to use that command :(")
		}
	} else if (msg.content.startsWith(discordConfig.prefix + 'fullinfo')) {
		if (msg.member.hasPermission('ADMINISTRATOR')) {
			(async () => {
				const channel = msg.channel
				console.log(`Processing a full search command ran by ${msg.member.user.tag}`)

				const parameter = msg.content.split('+').slice(1).shift()
				msg.delete()
				try {
					const entry = await fetchEntryInfo(parameter)
					await channel.send('```json\n' + JSON.stringify(entry, null, 2) + '```')
				} catch (exception) {
					console.error(exception)
					msg.channel.send('Entry doesn\'t exist :(')
				}
			})()
		} else {
			msg.reply("I'm sorry. Only administrators are allowed to use that command :(")
		}
	} else if (msg.content === discordConfig.prefix + 'options') {
		if (msg.member.hasPermission('ADMINISTRATOR')) {
			console.log(`Displaying available options to ${msg.member.user.tag}`)
			let message = '**Mapped values are:** \n'
			entryTypeMap.forEach((value, key) => {
				message += key + ' -> ' + value + '\n'
			})
			message += 'Current mode is: ' + (process.env.NODE_ENV || 'dev')
			msg.reply(message)
		}
	} else if (msg.content.startsWith(discordConfig.prefix + 'poll')) {
		(async () => {
			try {
				var pattern = /".*?"/g
				var message = msg.content.slice(6)
				console.log(`${msg.member.user.tag} createad a poll for ${message}`)
				var values = []
				var current
				while ((current = pattern.exec(message)) !== null) {
					values.push(current[0].substring(1, current[0].length - 1))
				}
				console.log(`Poll values: ${values}`)
				msg.delete()
				if (values.length > 2) {
					var answers = values.slice(1)
					var embed = await generatePollEmbed(msg.member.user, values[0], answers)
					var sentMessage = await msg.channel.send(embed)
					for (let i = 0; i < answers.length; i++) {
						await sentMessage.react(icons[i])
					}
					shuffle(icons)
				} else {
					msg.reply('You have to provide a question and at least two answers')
				}
			} catch (exception) {
				console.error(exception)
				msg.channel.send('Missing permissions')
			}
		})()
	} else if (msg.content === discordConfig.prefix + 'smile') {
		msg.reply('😄')
	} else if (!msg.author.bot && goodNight.findIndex(i => msg.content.toLowerCase().startsWith(i)) > -1) {
		msg.channel.send('ć')
	}
})

client.on('error', function (error) {
	console.error(`Client error: ${error}`)
})

client.on('reconnecting', function () {
	console.log('Client connected to WebSocket.')
})

client.on('resume', function (replayed) {
	console.log(`Resumed WebSocket. ${replayed} events were replayed.`)
})

client.on('disconnect', function (event) {
	console.log(`The WebSocket has closed with the event ${event}, and will no longer attempt to reconnect.`)
})

client.login(discordConfig.token)
